# README #

I used to run an event photography business, and each athlete in a running race, for example, is assgined a bib number.  The goal for me was to identify who were in each image,
and viewers should be able to search by bib number or last name.  

I created an OSX app called Zekken, meaning bib number in Japanese, to achieve the goal.  In the program I went through each picture, and punch number(s).  At the end of 
this process, I would get something like this:

| Image File	| Bib Number	|
| --------------|-------------	|
| IMG_0001.jpg	| 12			|
| IMG_0002.jpg	| 789			|
| IMG_0003.jpg	| 43			|
| IMG_0004.jpg	| 90, 42		|
| IMG_0005.jpg	| 87, 45, 31	|

And then I got another list that showed who had which number:

| Bib Number	| Last Name		|
| --------------|-------------	|
|		12		| Johnson		|
|		31		| Mason			|
|		42		| Sheehan		|
|		43		| Suzuki		|
|		45		| Hodgeson		|
|		87		| Nichols		|
|		90		| Lee			|

Finally I needed to combine these 2 and generate something like this:

| Image File	| Keywords 								|
| --------------|-------------------------------------- |
| IMG_0001.jpg	| 12, Johnson							|
| IMG_0002.jpg	| 789									|
| IMG_0003.jpg	| 43, Suzuki 							|
| IMG_0004.jpg	| 90, 42, Lee, Sheehan					|
| IMG_0005.jpg	| 87, 45, 31, Nichols, Hodgeson, Mason	|

The original Zekken, written in Objective-C, has way more features than this repo, and therefore far more complicated.  This repo is an extract from it, and it only generates the last list.

The reason why I created this repo is to 1) show that I can create apps in *Swift*, and 2) help my photographer friend.