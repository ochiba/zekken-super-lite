//
//  ViewController.swift
//  Zekken Super Lite
//
//  Created by Osamu Chiba on 7/22/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSOpenSavePanelDelegate {
    
    @IBOutlet weak var textDestination: NSTextField?
    @IBOutlet weak var textImageSource: NSTextField?
    @IBOutlet weak var textNameSource: NSTextField?
    var messageController: NSAlert?;

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {
        }
    }

    @IBAction func browseImageFile(_ sender: Any?)
    {
        sourceFile(true)
    }

    @IBAction func browseNameFile(_ sender: Any?)
    {
        sourceFile(false)
    }
    
    func sourceFile(_ isImage: Bool)
    {
        let filePanel                       = NSOpenPanel();
        filePanel.title                     = "Choose a file to import";
        filePanel.canChooseDirectories      = false;
        filePanel.canChooseFiles            = true;
        filePanel.canCreateDirectories      = false;
        filePanel.allowsMultipleSelection   = false;
        
        if (filePanel.runModal() == NSModalResponseOK)
        {
            let folder = filePanel.url;
            
            if (isImage)
            {
                self.textImageSource?.stringValue   = (folder?.path)!;
            }
            else
            {
                self.textNameSource?.stringValue    = (folder?.path)!;
            }
        }
    }
    
    @IBAction func browseOutputFile(_ sender: Any?)
    {
        let saveSQLPanel    = NSSavePanel();
        saveSQLPanel.title  = "Specify a file to save";
        
        if (saveSQLPanel.runModal() == NSModalResponseOK)
        {
            let folder = saveSQLPanel.url;
            self.textDestination?.stringValue = (folder?.path)!;
        }
    }

    @IBAction func go(_ sender: Any?)
    {
        var output          = "";
        var lastNameArray   = [String]();
        var bibArray        = [Int]();
        
        var imageNameIndex  = -1;
        var tagIndex        = -1;
        
        var bibIndex        = -1;
        var lastIndex       = -1;
        
        let imageSourceFile = (self.textImageSource?.stringValue)!;
        let nameSourceFile  = (self.textNameSource?.stringValue)!;
        let outputFile      = (self.textDestination?.stringValue)!;
        
        if (imageSourceFile == "" || nameSourceFile == "" || outputFile == "") {
            showMessage("All files must be filled.");
            return;
        }
        
        do {
            let imageFileContents       = try NSString(contentsOfFile: imageSourceFile,
                                                 encoding:String.Encoding.utf8.rawValue);
            
            let imageFileContentsArray  = imageFileContents.components(separatedBy: CharacterSet.newlines);
            let imageFileheader         = imageFileContentsArray[0].components(separatedBy: ";");
            
            // Image file should have 1) "Image Name" and 2) "TAGS" in the header.
            var result          = findIndexes(imageFileheader, key1: "TAGS", key2: "Image Name");
            tagIndex            = result.index1;
            imageNameIndex      = result.index2;
            
            if (imageNameIndex == -1 || tagIndex == -1) {
                showMessage("Image Name and / or Tag is missing.");
                // The image Name column isn't used for anything other than verifying that's the image source file.
                return;
            }
            
            let nameFileContents        = try NSString(contentsOfFile: nameSourceFile,
                                                 encoding:String.Encoding.utf8.rawValue);

            let nameFileContentsArray   = nameFileContents.components(separatedBy: CharacterSet.newlines);
            let nameFileheader          = nameFileContentsArray[0].components(separatedBy: ";");

            // Name file should have 1) "BIB" and 2) "Last" in the header.
            result          = findIndexes(nameFileheader, key1: "BIB", key2: "Last");
            bibIndex        = result.index1;
            lastIndex       = result.index2;
            
            if (bibIndex == -1 || lastIndex == -1) {
                showMessage("Bib and / or Last is missing.");
                return;
            }
        
            // Create 2 arrays, one for bib and the other for last name.  These will be used as references.
            for i in 1..<nameFileContentsArray.count {
                
                let nameFile1Line       = nameFileContentsArray[i].components(separatedBy: ";");
                
                if (nameFile1Line.count > 1) {

                    let tempoBib        = nameFile1Line[bibIndex].replacingOccurrences(of: " ", with: "");
                    let tempoLastName   = nameFile1Line[lastIndex];
                    
                    if (tempoBib != "" && tempoLastName != "")
                    {
                        lastNameArray.append(tempoLastName);
                        bibArray.append(Int(tempoBib)!);
                    }
                }
            }
            
            // Add header with new column, Keywords.
            output = imageFileContentsArray[0] + ";Keywords\n";
            
            for i in 1..<imageFileContentsArray.count {

                let imageFile1Line     = imageFileContentsArray[i].components(separatedBy: ";");
                
                if (imageFile1Line.count > 1) {
                    
                    var keywords    = imageFile1Line[tagIndex];
                    let tagCell     = keywords.replacingOccurrences(of: " ", with: "").components(separatedBy: ",");
                    
                    for j in 0..<tagCell.count {
                        
                        if (tagCell[j] != "") {
                            let temoBib = Int(tagCell[j])!;
                            
                            if let k = bibArray.index(where: { $0 == temoBib }) {
                                keywords += ", " + lastNameArray[k];
                            }
                        }
                    }

                    output += imageFileContentsArray[i] + ";" + keywords + "\n";
                }
            }

            let filemgr = FileManager.default;
            let data    = output.data(using: String.Encoding.utf8);
            
            if (filemgr.createFile(atPath: outputFile, contents:data, attributes: nil)) {
                showMessage("Success!");
            }
            else {
                showMessage("Failed...");
            }

        }catch {
            showMessage("Something went wrong.");
        }
    }

    @IBAction func close(_ sender: Any?)
    {
        let app = NSApplication.shared;
        app().terminate(nil);
    }
    
    func showMessage(_ newMesage: String)
    {
        let alert           = NSAlert();
        alert.messageText   = newMesage;
        alert.runModal();
    }
    
    func findIndexes(_ headerArray: [String], key1 keyword1: String, key2 keyword2: String) -> (index1: Int, index2: Int)
    {
        var index1 = -1;
        var index2 = -1;
        
        for i in 0..<headerArray.count {
            
            if (headerArray[i].caseInsensitiveCompare(keyword1) == ComparisonResult.orderedSame)
            {
                index1 = i;
            }
            
            if (headerArray[i].caseInsensitiveCompare(keyword2) == ComparisonResult.orderedSame)
            {
                index2 = i;
            }
        }
        return (index1, index2);
    }
}

