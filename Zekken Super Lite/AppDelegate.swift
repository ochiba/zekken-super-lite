//
//  AppDelegate.swift
//  Zekken Super Lite
//
//  Created by Osamu Chiba on 7/22/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

